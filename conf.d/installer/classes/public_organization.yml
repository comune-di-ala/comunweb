identifier: public_organization
contentobject_name: '<legal_name>'
serialized_name_list:
    always-available: ita-IT
    ita-IT: 'Organizzazione pubblica'
    ger-DE: 'Öffentliche Körperschaft'
serialized_description_list:
    ita-IT: 'Una pubblica amministrazione e qualsiasi altra organizzazione di natura pubblica, registrata presso l''Indice della PA (IPA)'
    ger-DE: 'Eine öffentliche Verwaltung und jede andere öffentliche Körperschaft, die im digitalisierten gesamtstaatlichen Verzeichnis IPA registriert ist'
always_available: 1
sort_field: 9
data_map:
    legal_name:
        identifier: legal_name
        serialized_description_list:
            ita-IT: 'Nome primario o il nome legale dell''organizzazione'
            ger-DE: 'Hauptname oder rechtlicher Name der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Nome
            ger-DE: Name
        data_type_string: ezstring
        placement: 1
        is_searchable: 1
        is_required: 1
        can_translate: 1
        serialized_data_text: {  }
    alt_name:
        identifier: alt_name
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'un nome alternativo dell''organizzazione (e.g., un nome colloquiale)'
            ger-DE: 'Alternativer Name der Körperschaft (z. B. ein umgangssprachlicher Name)'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Nome alternativo'
            ger-DE: 'Alternativer Name'
        data_type_string: ezstring
        placement: 2
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    acronym:
        identifier: acronym
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'eventuale acronimo dell''organizzazione'
            ger-DE: 'Etwaiges Kürzel der Körperschaft angeben'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Acronimo
            ger-DE: Abkürzung
        data_type_string: ezstring
        placement: 3
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    description:
        identifier: description
        serialized_description_list:
            ita-IT: 'Descrizione completa dell''organizzazione'
            ger-DE: 'Detaillierte Beschreibung der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Descrizione
            ger-DE: Beschreibung
        data_type_string: ezxmltext
        placement: 4
        is_required: 1
        can_translate: 1
        data_int1: 5
        serialized_data_text: {  }
    main_function:
        identifier: main_function
        serialized_description_list:
            ita-IT: 'Funzione dell''organizzazione ed elenco dei compiti assegnati'
            ger-DE: 'Funktion der Körperschaft und Liste der zugewiesenen Aufgaben'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Competenze e funzioni'
            ger-DE: 'Zuständigkeiten und Funktionen'
        data_type_string: ezxmltext
        placement: 5
        can_translate: 1
        data_int1: 5
        serialized_data_text: {  }
    has_logo:
        identifier: has_logo
        serialized_description_list:
            ita-IT: 'Logo dell''organizzazione'
            ger-DE: 'Logo der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Logo
            ger-DE: Logo
        data_type_string: ezimage
        placement: 6
        can_translate: 1
        serialized_data_text: {  }
    image:
        identifier: image
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Immagine/i principale/i dell''organizzazione'
            ger-DE: 'Hauptbild oder Logo der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Immagini
            ger-DE: Bild
        data_type_string: ezobjectrelationlist
        placement: 7
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - image
            default_placement:
                node_id: $content_Images_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    start_time:
        identifier: start_time
        serialized_description_list:
            ita-IT: 'Data a partire dalla quale l''organizzazione è attiva'
            ger-DE: 'Tätigkeitsbeginn der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data inizio validità'
            ger-DE: 'Gültig/wirksam ab '
        data_type_string: ezdate
        placement: 8
        can_translate: 1
        serialized_data_text: {  }
    end_time:
        identifier: end_time
        serialized_description_list:
            ita-IT: 'Data di fine attività dell''organizzazione'
            ger-DE: 'Datum an dem die Körperschaft ihre Tätigkeit beendet'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data fine validità'
            ger-DE: 'Gültig/wirksam bis '
        data_type_string: ezdate
        placement: 9
        can_translate: 1
        serialized_data_text: {  }
    has_spatial_coverage:
        identifier: has_spatial_coverage
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Sedi
            ger-DE: Sitz/Adresse
        data_type_string: ezobjectrelationlist
        placement: 10
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - place
            default_placement:
                node_id: $contenttree_Amministrazione_Luoghi_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    accreditation_date:
        identifier: accreditation_date
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'data di accreditamento dell''organizzazione nel pubblico registro IndicePA'
            ger-DE: 'Datum der Akkreditierung der Körperschaft im öffentlichen Register IndicePA'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data di accreditamento'
            ger-DE: 'Datum der Akkreditierung'
        data_type_string: ezdate
        placement: 11
        is_searchable: 1
        can_translate: 1
        category: meta
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    has_online_contact_point:
        identifier: has_online_contact_point
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Punti di contatto'
            ger-DE: Kontaktstellen
        data_type_string: ezobjectrelationlist
        placement: 12
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - online_contact_point
            default_placement:
                node_id: $contenttree_Classificazioni_Punti-di-contatto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    foundation_date:
        identifier: foundation_date
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'la data di costituzione dell''organizzazione'
            ger-DE: 'Gründungsdatum der Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Data di costituzione'
            ger-DE: 'Gründungsdatum '
        data_type_string: ezdate
        placement: 13
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    has_public_org_activity_type:
        identifier: has_public_org_activity_type
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Tipo di attività'
            ger-DE: Tätigkeitsbereich
        data_type_string: eztags
        placement: 14
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / COFOG)'
        data_int3: 1
        data_int4: 2
        data_text1: Tree
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    topics:
        identifier: topics
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'tematiche trattate da questa organizzazione'
            ger-DE: 'Themen, die von dieser Körperschaft behandelt werden'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Argomenti
            ger-DE: Themen
        data_type_string: ezobjectrelationlist
        placement: 15
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '3'
            type: '2'
            class_constraint_list:
                - topic
            default_placement:
                node_id: $contenttree_OpenCity_Argomenti_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    assessore:
        identifier: assessore
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Assessore di riferimento'
            ger-DE: 'Zuständiger Stadtrat'
        data_type_string: ezobjectrelationlist
        placement: 16
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - politico
            default_placement:
                node_id: $contenttree_Amministrazione_Politici_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    servizi_offerti:
        identifier: servizi_offerti
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Elenco dei servizi offerti'
            ger-DE: 'Verzeichnis der angebotenen Dienste'
        data_type_string: ezobjectrelationlist
        placement: 17
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list: {  }
            default_placement:
                node_id: $contenttree_OpenCity_Servizi_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    attachments:
        identifier: attachments
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Allegati
            ger-DE: Anhänge
        data_type_string: ezobjectrelationlist
        placement: 18
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - document
            default_placement:
                node_id: $contenttree_Documenti-e-dati_Documenti-tecnici-di-supporto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    more_information:
        identifier: more_information
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Ulteriori informazioni sulla struttura non contemplate dai campi precedenti'
            ger-DE: 'Zusätzliche Informationen über die Struktur, die nicht durch die vorherigen Felder abgedeckt sind'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Ulteriori informazioni'
            ger-DE: 'Weitere Informationen'
        data_type_string: ezxmltext
        placement: 19
        is_searchable: 1
        can_translate: 1
        data_int1: 5
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    help:
        identifier: help
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Eventuali contatti di supporto all''utente. Per esempio i riferimenti dell''URP'
            ger-DE: 'Mögliche Supportkontakte für den Benutzer. Zum Beispiel  Bürgerschalter'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Per informazioni'
            ger-DE: 'Weitere Informationen'
        data_type_string: ezobjectrelationlist
        placement: 20
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - online_contact_point
            default_placement:
                node_id: $contenttree_Classificazioni_Punti-di-contatto_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    identifier:
        identifier: identifier
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Codice stabilito dal Comune, con validità interna'
            ger-DE: 'Von der Stadtverwaltung erstellter Kodex mit interner Gültigkeit'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Identificativo univoco interno'
            ger-DE: 'Eindeutiger  Amtserkennungscode '
        data_type_string: ezstring
        placement: 21
        is_searchable: 1
        can_translate: 1
        category: meta
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    support_unit:
        identifier: support_unit
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Area, dipartimento, ripartizione'
            ger-DE: 'Bereich, Ressort, Abteilung'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: Area
            ger-DE: Bereich
        data_type_string: ezobjectrelationlist
        placement: 22
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - administrative_area
            default_placement:
                node_id: $contenttree_Amministrazione_Aree-amministrative_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    holds_role_in_time:
        identifier: holds_role_in_time
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Riveste un ruolo nel tempo'
            ger-DE: 'Übernimmt im Laufe der Zeit eine Funktion'
        data_type_string: ezobjectrelationlist
        placement: 23
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - time_indexed_role
            default_placement:
                node_id: $content_Ruoli_node
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    tax_code:
        identifier: tax_code
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Codice fiscale'
            ger-DE: Steuernumer
        data_type_string: ezstring
        placement: 24
        is_searchable: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    type:
        identifier: type
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Indicare la tipologia tradizionale di struttura organizzativa del Comune'
            ger-DE: 'Istitutionelle Organisationsstruktur der Stadtverwaltung'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Tipo di struttura organizzativa'
            ger-DE: Organisationsstruktur
        data_type_string: eztags
        placement: 25
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / Tipo di struttura organizzativa)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        category: hidden
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    ipacode:
        identifier: ipacode
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Questa proprietà rappresenta il codice IPA, il codice univoco, assegnato all''organizzazione pubblica in fase di accreditamento presso l''indice nazionale PA (IPA). Il Codice IPA viene comunicato tramite e-mail dal Gestore dell''IPA al referente dell''Ente.'
            ger-DE: 'Öffentliche Körperschaft'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Codice IPA'
            ger-DE: IPA-Code
        data_type_string: ezstring
        placement: 26
        is_searchable: 1
        is_required: 1
        can_translate: 1
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    public_organization_category:
        identifier: public_organization_category
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Categoria di organizzazione pubblica'
            ger-DE: 'Kategorie der öffentlichen Körperschaft'
        data_type_string: eztags
        placement: 27
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Servizi pubblici / NACE / 64.1 - Intermediazione monetaria)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    is_public_organization_of:
        identifier: is_public_organization_of
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'E'' un''organizzazione pubblica appartenente a'
            ger-DE: 'Diese öffentliche Körperschaft gehört zu...'
        data_type_string: ezobjectrelationlist
        placement: 28
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list: {  }
            default_placement: false
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    legal_status_code:
        identifier: legal_status_code
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'codice della forma giuridica'
            ger-DE: 'Kodex Rechtsform'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Forma giuridica'
            ger-DE: Rechtsform
        data_type_string: eztags
        placement: 29
        is_searchable: 1
        can_translate: 1
        data_int1: 'tag(Organizzazione / Legal status pubblico)'
        data_int3: 1
        data_int4: 1
        data_text1: Tree
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    participating:
        identifier: participating
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Indicare le organizzazione a cui questa organizzazione partecipa'
            ger-DE: 'Geben Sie die Organisationen an, an der diese Körperschaft beteiligt ist'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Partecipa '
            ger-DE: Beteiligung
        data_type_string: ezobjectrelationlist
        placement: 30
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - office
            default_placement: false
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    has_balance_sheet:
        identifier: has_balance_sheet
        serialized_description_list:
            always-available: ita-IT
            ita-IT: 'Inserire uno o più bilanci'
            ger-DE: 'Ein oder mehrere Haushalte/Bilanzen einfügen'
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Bilanci dell''organizzazione'
            ger-DE: 'Haushalt/Bilanz der Körperschaft'
        data_type_string: ezobjectrelationlist
        placement: 31
        is_searchable: 1
        can_translate: 1
        data_text5:
            object_class: ''
            selection_type: '0'
            type: '2'
            class_constraint_list:
                - document
            default_placement: false
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
    people:
        identifier: people
        serialized_description_list:
            always-available: ita-IT
            ita-IT: ''
        serialized_name_list:
            always-available: ita-IT
            ita-IT: 'Persone che compongono la struttura'
            ger-DE: 'Mitarbeiterinnen und Mitarbeiter'
        data_type_string: openparole
        placement: 32
        can_translate: 1
        data_text5: '{"roletype_root_tag":"Persone\/Ruoli","select":2,"view":3,"filter":["Segretario comunale","Sindaco","Vicesindaco"],"sort":1}'
        serialized_data_text:
            always-available: ita-IT
            ita-IT: ''
groups:
    - 'Organizzazione (COV)'
